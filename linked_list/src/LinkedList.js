function Node(value, previous, next) {
    this.value = value;
    this.previous = previous;
    this.next = next;
}

function LinkedList() {
    this.size = 0;
    this.root = new Node();
}

LinkedList.prototype.add = function(value) {
    var node = this.root;
    while (node.next !== undefined) {
        node = node.next;
    }
    node.next = new Node(value, node);
    this.size++;
};

LinkedList.prototype.remove = function(index) {
    var node = this.getNodeAtIndex(index);
    node.previous.next = node.next;
    if (node.next !== undefined) {
        node.next.previous = node.previous;
    }
    this.size--;
};

LinkedList.prototype.get = function(index) {
    var node = this.getNodeAtIndex(index);
    return node.value;
};

LinkedList.prototype.getNodeAtIndex = function(index) {
    var node = this.root;
    for (var i = 0; i <= index; i++) {
        node = node.next;
    }
    return node;
};

LinkedList.prototype.forEach = function(func) {
    var node = this.root;
    while (node.next !== undefined) {
        node = node.next;
        func(node.value);
    }
};

LinkedList.prototype.map = function(func) {
    var newLinkedList = new LinkedList();
    this.forEach(function(value) {
        newLinkedList.add(func(value));
    });
    return newLinkedList;
};

LinkedList.prototype.filter = function(func) {
    var newLinkedList = new LinkedList();
    this.forEach(function(value) {
        if (func(value)) {
            newLinkedList.add(value);
        }
    });
    return newLinkedList;
};
