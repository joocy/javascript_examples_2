describe("A linked list", function() {

    var linkedList;

    function linkedListForList(list) {
        var linkedList = new LinkedList();
        list.forEach(function(value) {
            linkedList.add(value);
        });
        return linkedList;
    }

    beforeEach(function(){
        linkedList = new LinkedList();
    });

    it("should have a size of 0 when created", function() {
        expect(linkedList.size).toBe(0);
    });

    it("should increment its size when a value is added", function() {
        linkedList.add("first");
        expect(linkedList.size).toBe(1);
        linkedList.add("second");
        expect(linkedList.size).toBe(2);
    });

    it("should decrement its size when a value is removed", function() {
        linkedList = linkedListForList(["first", "second"]);
        linkedList.remove(0);
        expect(linkedList.size).toBe(1);
        linkedList.remove(0);
        expect(linkedList.size).toBe(0);
    });

    it("should retrieve values that are added", function() {
        linkedList.add("first");
        expect(linkedList.get(0)).toBe("first");
        linkedList.add("second");
        expect(linkedList.get(1)).toBe("second");
    });

    it("should remove values when requested", function() {
        linkedList = linkedListForList(["first", "second"]);
        linkedList.remove(0);
        expect(linkedList.get(0)).toBe("second");
    });

    it("should call a function for every value when forEach is called", function() {
        linkedList = linkedListForList(["first", "second", "third"]);

        spyOn(console, "log");

        linkedList.forEach(function(value) { console.log(value); });

        expect(console.log).toHaveBeenCalledWith("first");
        expect(console.log).toHaveBeenCalledWith("second");
        expect(console.log).toHaveBeenCalledWith("third");

        expect(console.log.calls.count()).toBe(3);
    });

    it("should return a new linked list when map is called", function() {
        var newLinkedList = linkedList.map(function(){});
        expect(newLinkedList).toBeDefined();
        expect(newLinkedList).not.toBe(linkedList);
    });

    it("should map a function on all values", function() {
        linkedList = linkedListForList([1,2,3]);
        var newLinkedList = linkedList.map(function(value){ return value * 2; });
        expect(newLinkedList.get(0)).toEqual(2);
        expect(newLinkedList.get(1)).toEqual(4);
        expect(newLinkedList.get(2)).toEqual(6);
    });

    it("should filter its values", function() {
        linkedList = linkedListForList([1,2,3]);
        var newLinkedList = linkedList.filter(function(value){ return value % 2 == 1; });
        expect(newLinkedList.get(0)).toEqual(1);
        expect(newLinkedList.get(1)).toEqual(3);
    });
});
