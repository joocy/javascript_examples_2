// this factory function returns
// a function that wraps the value of
// the mod parameter. this is called
// a closure.
function modFactory(mod) {

    return function(x) {
        return x % mod === 0;
    };

}

// a function is javascript is not
// limited to accept the parameters
// that are declared in the arguments
// list
function fizzbuzz() {
    var lower, upper;
    if (arguments.length === 1) {
        lower = 1;
        upper = arguments[0];
    }
    else if (arguments.length === 2) {
        lower = arguments[0];
        upper = arguments[1];
    }

    var isDivisibleByThree = modFactory(3);
    var isDivisibleByFive = modFactory(5);

    for (var i = lower; i <= upper; i++) {

        var result = "";

        if (isDivisibleByThree(i)) {
            result += "Fizz";
        }

        if (isDivisibleByFive(i)) {
            result += "Buzz";
        }

        console.log((result.length > 0) ? result : i);
    }
}

// the function uses one argument as the upper bound
fizzbuzz(15);

// with two arguments, the function uses a lower ad upper bound
fizzbuzz(10, 15);
